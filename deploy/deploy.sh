#!/usr/bin/env bash

tar -cvzf gesture.tar.gz ../src/*

nvidia-docker stop gesture_container
nvidia-docker rm gesture_container
nvidia-docker build --network host -t gesture_image .
nvidia-docker run -d -p 50052:50052 --name gesture_container gesture_image
rm -rf ./gesture.tar.gz