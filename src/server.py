import numpy as np
import os, sys
import time
import grpc
from concurrent import futures
import cv2

# grpc things
from pbs import action_pb2, action_pb2_grpc

from gesture.TFModel import TFModel

# add to python path
gesture_home = os.path.join(os.path.dirname(os.path.abspath(__file__)),'gesture')
sys.path.insert(0, gesture_home)

from darknet.python.darknet import *

class TF_GestureModel:
    def __init__(self):
        self.tf_model = TFModel()
        self.video_stream = []
        self.action = 'None'
        self.confidence = 1.0

    def run(self, frame):
        self.video_stream.append(frame)
        if len(self.video_stream) == self.tf_model.video_length:
            action, confidence = self.tf_model.run(np.array([self.video_stream]))
            self.action = action
            self.confidence = confidence
            self.video_stream = []

def main():
    model = TF_GestureModel()

    class Greeter(action_pb2_grpc.GreeterServicer):
        def __init__(self):
            super(Greeter,self).__init__()
            self.init_box(-0.5,-0.5, -1, -1)

        def init_box(self, x, y, w, h):
            self.x, self.y, self.w, self.h = x, y, w, h

        def Analyze(self, request_iterator, context):
            print "Invoked"
            for req in request_iterator:
                frame = np.frombuffer(req.frame, dtype=np.uint8).reshape(480, 640, 3)

                if not os.path.exists('./gesture/tmp'):
                    os.makedirs('./gesture/tmp')
                cv2.imwrite('./gesture/tmp/0.jpg', frame)

                r = detect(model.tf_model.yolo, model.tf_model.meta, './gesture/tmp/0.jpg')

                if r and not model.video_stream:
                    _, _, (x, y, w, h) = r[0]  # coco
                    self.init_box(x,y,w,h)

                ymin, ymax, xmin, xmax = int(self.y - self.h / 2), int(self.y + self.h / 2), int(self.x - self.w / 2), int(self.x + self.w / 2)

                if ymin > 0 and xmin > 0 \
                        and ymin < ymax and xmin < xmax:
                    frame = frame[ymin:ymax, xmin:xmax]

                # aspect ratio
                ratio = float(frame.shape[0]) / float(frame.shape[1])

                if ratio > 1.0:  # sitting
                    frame = frame[:int(frame.shape[0] / 2)]

                elif ratio > 1.5:  # standing
                    frame = frame[:int(frame.shape[0] / 2.5)]

                print frame.shape

                frame = cv2.resize(frame, (132, 100))


                # results from DL model
                model.run(frame)

                tmp_action = model.action
                tmp_confidence = model.confidence

                model.action = 'None'
                model.confidence = 1.0

                print tmp_action, tmp_confidence

                if tmp_action != 'None':
                    self.init_box(-0.5, -0.5, -1, -1)

                return action_pb2.MsgReply(action=tmp_action,
                                           confidence=tmp_confidence)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    action_pb2_grpc.add_GreeterServicer_to_server(Greeter(), server)
    server.add_insecure_port('[::]:50052')
    server.start()
    try:
        while True:
            time.sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        server.stop(0)

if __name__=='__main__':
    main()
