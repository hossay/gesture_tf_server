import tensorflow as tf

class LSTM_Action:
    def __init__(self, n_hidden, n_class, batch_size):
        self.inputs = tf.placeholder(dtype=tf.float32, shape=[batch_size, None, None, None, 3], name='inputs') # (batch, time, h, w, c)
        self.targets = tf.placeholder(tf.int32, [batch_size], name='targets')
        self.training = tf.placeholder(tf.bool, name='training') # for BN

        _shape = tf.shape(self.inputs)
        l,h,w = tf.unstack(_shape[1:-1])

        # first, crop it randomly
        crop_inputs = tf.cond(self.training,
                              lambda: tf.random_crop(self.inputs, tf.unstack(_shape[:2]) + [100, 100, 3]),
                              lambda: self.inputs[:, :, (h - 100) / 2:(h + 100) / 2, (w - 100) / 2:(w + 100) / 2])
        crop_inputs = tf.reshape(crop_inputs, (batch_size, -1, 100, 100, 3))  # for channel dimension restore

        processed_inputs = []
        for _in in tf.unstack(crop_inputs):
            def preprocess(t,seq_img):
                img = tf.image.per_image_standardization(_in[t]) # standardization
                seq_img = seq_img.write(t,img)

                return t+1, seq_img

            t = tf.constant(0)
            seq_img = tf.TensorArray(dtype=tf.float32, size=l)

            _, seq_img = tf.while_loop(cond=lambda t,*_ : t<l,
                                       body=preprocess, loop_vars=(t,seq_img))

            processed_inputs.append(seq_img.stack())

        def batch_norm(inputs, training, name, device):
            with tf.device(device):
                inputs_norm = tf.layers.batch_normalization(inputs=inputs,
                                                            training=training, name=name)
            return inputs_norm

        self.summary_op = tf.summary.image('summary_op', tf.stack(processed_inputs)[:,0])

        # conv1
        conv1 = tf.layers.conv3d(inputs=tf.stack(processed_inputs), filters=64, kernel_size=(3,3,3), padding='same', name='conv1')
        conv1 = batch_norm(inputs=conv1, training=self.training, name='conv1_BN', device='/gpu:0')
        conv1 = tf.nn.relu(conv1, name='conv1_relu')

        # conv2
        conv2 = tf.layers.conv3d(inputs=conv1, filters=32, kernel_size=(1, 1, 1), padding='same', name='conv2')
        conv2 = batch_norm(inputs=conv2, training=self.training, name='conv2_BN', device='/gpu:0')
        conv2 = tf.nn.relu(conv2, name='conv2_relu')

        # pool1
        pool1 = tf.layers.max_pooling3d(inputs=conv2, pool_size=(1,2,2), strides=(1,2,2), name='pool1')

        # conv3
        conv3 = tf.layers.conv3d(inputs=pool1, filters=128, kernel_size=(3, 3, 3), padding='same', name='conv3')
        conv3 = batch_norm(inputs=conv3, training=self.training, name='conv3_BN', device='/gpu:0')
        conv3 = tf.nn.relu(conv3, name='conv3_relu')

        # conv4
        conv4 = tf.layers.conv3d(inputs=conv3, filters=64, kernel_size=(1, 1, 1), padding='same', name='conv4')
        conv4 = batch_norm(inputs=conv4, training=self.training, name='conv4_BN', device='/gpu:0')
        conv4 = tf.nn.relu(conv4, name='conv4_relu')

        # pool2
        pool2 = tf.layers.max_pooling3d(inputs=conv4, pool_size=(1, 2, 2), strides=(1, 2, 2), name='pool2')

        # conv5
        conv5 = tf.layers.conv3d(inputs=pool2, filters=256, kernel_size=(3, 3, 3), padding='same', name='conv5')
        conv5 = batch_norm(inputs=conv5, training=self.training, name='conv5_BN', device='/gpu:0')
        conv5 = tf.nn.relu(conv5, name='conv5_relu')

        # conv6
        conv6 = tf.layers.conv3d(inputs=conv5, filters=128, kernel_size=(1, 1, 1), padding='same', name='conv6')
        conv6 = batch_norm(inputs=conv6, training=self.training, name='conv6_BN', device='/gpu:0')
        conv6 = tf.nn.relu(conv6, name='conv6_relu')

        # pool3
        pool3 = tf.layers.max_pooling3d(inputs=conv6, pool_size=(1, 2, 2), strides=(1, 2, 2), name='pool3')

        input_sequence = tf.reduce_mean(pool3, axis=(2,3))

        cell = tf.nn.rnn_cell.LSTMCell(n_hidden)

        # 'outputs' is a tensor of shape [batch_size, max_time, 256]
        # 'state' is a N-tuple where N is the number of LSTMCells containing a
        # tf.contrib.rnn.LSTMStateTuple for each cell
        outputs, state = tf.nn.dynamic_rnn(cell=cell,
                                           inputs=input_sequence,
                                           dtype=tf.float32)

        self.softmax = tf.nn.softmax(tf.layers.dense(outputs, n_class), name='softmax')
        self.softmax_avg = tf.reduce_mean(self.softmax, axis=1)