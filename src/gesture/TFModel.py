import tensorflow as tf
import numpy as np
import os, sys

gesture_home = os.path.join(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, gesture_home)

import model_zoo
import os
from darknet.python.darknet import *

with open('./gesture/category.txt') as f:
    lines = map(lambda x: x.strip(), f.readlines())

ix2label = dict(zip(range(len(lines)), lines))

model_path = os.path.join('gesture', 'save_model', 'jester-gray')

class TFModel:
    def __init__(self, video_length=30, FPS=15.0):
        self.video_length = video_length
        self.FPS = FPS

        # load yolo v3(tiny) and meta data
        # self.yolo = load_net("./gesture/darknet/cfg/yolov3-tiny-voc.cfg", "./gesture/darknet/cfg/yolov3-tiny-voc_210000.weights", 0)
        # self.meta = load_meta("./gesture/darknet/cfg/voc.data")
        self.yolo = load_net("./gesture/darknet/cfg/yolov3.cfg", "./gesture/darknet/cfg/yolov3.weights", 0)
        self.meta = load_meta("./gesture/darknet/cfg/coco.data")

        # define model
        self.action_model = model_zoo.LSTM_Action(batch_size=1,
                                                  n_class=13,
                                                  n_hidden=256)

        # open session
        self.sess = tf.Session(config=tf.ConfigProto(
            allow_soft_placement=True, log_device_placement=False))
        self.sess.run(tf.global_variables_initializer())

        saver = tf.train.Saver()

        ckpt = os.path.join(model_path,'step-{}'.format(16))

        print 'restore from {}...'.format(ckpt)
        saver.restore(self.sess, ckpt)

    def run(self, frames):
        predictions = self.sess.run(self.action_model.softmax, feed_dict={self.action_model.inputs: frames,
                                                                          self.action_model.training: False})

        mask = map(lambda x: int(ix2label[int(x.argmax(axis=-1))]!='Doing other things'), predictions[0])

        # casting
        mask = np.expand_dims(np.expand_dims(mask, axis=0), 2)

        predicted_label = map(lambda x: ix2label[int(x.argmax(axis=-1))], predictions[0])

        if predicted_label.count('Doing other things') > int(self.video_length*0.8):
            return 'Doing other things', 1.0

        # apply mask to predictions
        predictions_masked = np.mean(mask*predictions, axis=1)

        str_prediction = ix2label[int(predictions_masked.argmax(axis=-1).squeeze())]

        return str_prediction.strip(), predictions_masked.max()