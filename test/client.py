import cv2
import numpy as np
import os, sys

sys.path.insert(0, os.path.abspath('../src'))

# grpc things
from pbs import action_pb2, action_pb2_grpc
import grpc
import pyrealsense2 as rs

class Client:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

        # setup for IR(realsense)
        self.pipeline = rs.pipeline()
        config = rs.config()

        # depth_aligned_to_color  rs.stream.depth
        config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 15)  # set depth
        config.enable_stream(rs.stream.infrared, 1, 640, 480, rs.format.y8, 15)  # right imager

        profile = self.pipeline.start(config)

        depth_sensor = profile.get_device().first_depth_sensor()
        depth_sensor.set_option(rs.option.emitter_enabled, 0)
        depth_sensor.set_option(rs.option.enable_auto_exposure, 1)

        for _ in range(30):
            self.pipeline.wait_for_frames()

    def generateRequests(self, frame):
        frame = frame.tobytes()
        yield action_pb2.MsgRequest(frame=frame, token='fuck')

    def send(self, frame):
        channel = grpc.insecure_channel('{}:{}'.format(self.ip, self.port))
        stub = action_pb2_grpc.GreeterStub(channel)
        response = stub.Analyze(self.generateRequests(frame))
        return response

    def run(self):
        while (True):
            frames = self.pipeline.wait_for_frames()

            depth_frame = frames.get_depth_frame()

            ir_frame1 = frames.get_infrared_frame(1)

            if not depth_frame or not ir_frame1:
                continue

            # convert depth images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())

            # convert infrared images to numpy arrays
            ir_image1 = np.asanyarray(ir_frame1.get_data())

            ir_image1 = cv2.cvtColor(ir_image1, cv2.COLOR_GRAY2BGR)
            frame = ir_image1

            cv2.imshow('video',frame)
            cv2.waitKey(1)

            # response from server
            response = self.send(frame)

            if response.action!='None' and response.confidence>0.3:
                return response.action, response.confidence