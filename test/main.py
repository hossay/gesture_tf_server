import Tkinter as tk
from Tkinter import PhotoImage

from PIL import Image
from PIL import ImageTk
import os, cv2
from natsort import natsorted
from threading import Thread
from Queue import Queue
import time
import numpy as np
from client import Client

class TV:
    def __init__(self, video_home='./video_repos'):
        #self.client = Client(ip="155.230.104.116", port=50051)
        self.client = Client(ip="localhost", port=50052)
        self.video_list = [ os.path.join(video_home, video) for video in natsorted(os.listdir(video_home)) ]
        self.maxvol = 10

        # shared variables
        self.ch = 0  # init channel number
        self.vol = 5 # init volume
        self.change = False
        self.is_play = True
        self.activate = False

        # queue for shared variables
        self.q = Queue()

        self.root = tk.Tk()
        self.root.geometry("+200+400")

        # initialize pannel
        self.blank_image = np.zeros((480, 640, 3), np.uint8)
        self.blank_image = Image.fromarray(self.blank_image)
        self.blank_image = ImageTk.PhotoImage(self.blank_image)

        self.tv_panel = tk.Label(self.root, image=self.blank_image)
        self.tv_panel.image = self.blank_image
        self.tv_panel.grid(row=0, column=0)

        self.state_pannel = tk.Label(self.root, text='gesture : {}\n'
                                                     'status : {}\n'
                                                     'channel : {}\n'
                                                     'volume : {}\n'
                                                     'confidence : {}\n'.format('None','off',self.ch,self.vol,'None'))

        self.state_pannel.config(font=("Courier", 20), width=35)
        self.state_pannel.grid(row=0, column=1)

        # set a callback to handle when the window is closed
        self.root.wm_title("SmartTV")
        self.root.wm_protocol("WM_DELETE_WINDOW", self.onClose)

        # for flag check
        self.th_loop = Thread(target=self.loop)        #self.th_loop.daemon = True
        self.th_loop.start()

        # for recog
        self.th_demo_loop = Thread(target=self.run_demo_wrapper)
        self.th_demo_loop.start()

    def loop(self):
        while True:
            if not self.q.empty():
                qval = self.q.get()
            cap = cv2.VideoCapture(self.video_list[self.ch])
            ret = False
            while True:
                if not self.q.empty():
                    qval = self.q.get()
                    change, vol, self.is_play = qval

                    if change:
                        break
                if self.is_play:
                    ret, np_frame = cap.read()
                if ret:
                    np_frame = cv2.resize(np_frame, (640,480))
                    np_frame = cv2.cvtColor(np_frame,cv2.COLOR_BGR2RGB)
                    frame = Image.fromarray(np_frame)
                    frame = ImageTk.PhotoImage(frame)

                    # simply update the panel
                    if self.is_play:
                        self.tv_panel.configure(image=frame)
                        time.sleep(33e-3)
                        self.tv_panel.image = frame
                else:
                    cap.set(cv2.CAP_PROP_POS_MSEC, 0.0)

            self.tv_panel.configure(image=self.blank_image)
            self.tv_panel.image = self.blank_image

            time.sleep(0.05)

    def run_demo_wrapper(self):
        while True:
            self.run_demo()
            time.sleep(0.05)

    def run_demo(self):
        result, confidence = self.client.run()

        if confidence>0.3:

            import subprocess

            if result=='Thumb Up':
                self.activate = True
                # subprocess.Popen(['mpg321','./sound_repos/System_Activated.mp3']).pid
                #os.system("mpg321 ./sound_repos/System_Activated.mp3")
                self.turn_on()
            if result == 'Thumb Down':
                self.activate = False
                # subprocess.Popen(['mpg321', './sound_repos/System_DeActivated.mp3']).pid
                #os.system("mpg321 ./sound_repos/System_DeActivated.mp3")
                self.turn_off()
            if self.activate:
                if result == 'Stop Sign':
                    # subprocess.Popen(['mpg321', './sound_repos/Stop.mp3']).pid
                    #os.system("mpg321 ./sound_repos/Stop.mp3")
                    self.stop()
                if result == 'Rolling Hand Backward':
                    # subprocess.Popen(['mpg321', './sound_repos/Keep_Going.mp3']).pid
                    #os.system("mpg321 ./sound_repos/Keep_Going.mp3")
                    self.play()

                if result == 'Swiping Up':
                    # subprocess.Popen(['mpg321', './sound_repos/Volume_Up_ThreeTime.mp3']).pid
                    #os.system("mpg321 ./sound_repos/Volume_Up.mp3")
                    self.vol_upup()
                if result == 'Swiping Down':
                    # subprocess.Popen(['mpg321', './sound_repos/Volume_Down_ThreeTime.mp3']).pid
                    #os.system("mpg321 ./sound_repos/Volume_Down.mp3")
                    self.vol_downdown()
                if result == 'Swiping Left':
                    # subprocess.Popen(['mpg321', './sound_repos/Channel_Down_ThreeTime.mp3']).pid
                    #os.system("mpg321 ./sound_repos/Channel_Down.mp3")
                    self.ch_downdown()
                if result == 'Swiping Right':
                    # subprocess.Popen(['mpg321', './sound_repos/Channel_Up_ThreeTime.mp3']).pid
                    #os.system("mpg321 ./sound_repos/Channel_Up.mp3")
                    self.ch_upup()

                if result=='Sliding Two Fingers Up':
                    # subprocess.Popen(['mpg321', './sound_repos/Volume_Up.mp3']).pid
                    #os.system("mpg321 ./sound_repos/Volume_Up.mp3")
                    self.vol_up()
                if result == 'Sliding Two Fingers Down':
                    # subprocess.Popen(['mpg321', './sound_repos/Volume_Down.mp3']).pid
                    #os.system("mpg321 ./sound_repos/Volume_Down.mp3")
                    self.vol_down()
                if result == 'Sliding Two Fingers Left':
                    # subprocess.Popen(['mpg321', './sound_repos/Channel_Down.mp3']).pid
                    #os.system("mpg321 ./sound_repos/Channel_Down.mp3")
                    self.ch_down()
                if result == 'Sliding Two Fingers Right':
                    # subprocess.Popen(['mpg321', './sound_repos/Channel_Up.mp3']).pid
                    #os.system("mpg321 ./sound_repos/Channel_Up.mp3")
                    self.ch_up()
                if result == 'Doing other things':
                    #os.system("mpg321 ./sound_repos/Try_it_again.mp3")
                    pass

            # update state pannel
            self.state_pannel.configure(text='gesture : {}\n'
                                             'channel : {}\n'
                                             'volume : {}\n'
                                             'confidence : {}\n'.format(result, self.ch, self.vol,confidence))



    def turn_on(self):
        self.q.put([self.change, self.vol, True])
    def turn_off(self):
        self.q.put([self.change, self.vol, True])
    def stop(self):
        self.is_play = False
        self.q.put([self.change, self.vol, False])
    def play(self):
        self.is_play = True
        self.q.put([self.change, self.vol, True])

    def vol_upup(self):
        self.vol = min(max(0,self.vol+3), self.maxvol)
        self.q.put([self.change, self.vol, True])
    def vol_downdown(self):
        self.vol = min(max(0,self.vol-3), self.maxvol)
        self.q.put([self.change, self.vol, True])
    def vol_up(self):
        self.vol = min(max(0,self.vol+1), self.maxvol)
        self.q.put([self.change, self.vol, True])
    def vol_down(self):
        self.vol = min(max(0,self.vol-1), self.maxvol)
        self.q.put([self.change, self.vol, True])

    def ch_upup(self):
        self.ch = (self.ch+3)%len(self.video_list)
        self.q.put([True, self.vol, True])
    def ch_downdown(self):
        self.ch = (self.ch-3)%len(self.video_list)
        self.q.put([True, self.vol, True])
    def ch_up(self):
        self.ch = (self.ch+1)%len(self.video_list)
        self.q.put([True, self.vol, True])
    def ch_down(self):
        self.ch = (self.ch-1)%len(self.video_list)
        self.q.put([True, self.vol, True])


    def onClose(self):
        # set the stop event, cleanup the camera, and allow the rest of
        # the quit process to continue
        print("[INFO] closing...")
        self.root.quit()

tv = TV()
tv.root.mainloop()
